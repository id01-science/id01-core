class GenericIndexTracker(object):
    """
    Just a matplotlibe widget that allows scrolling through
    several images and picking a point.
    No connection to spec.
    """

    _axes_properties = {}

    def __init__(
        self,
        ax,
        data=None,
        norm="linear",
        quantum=1.0,
        exit_onclick=False,
        rectangle_onclick=False,
        imshow_kw={},
    ):

        self.ax = ax
        self.fig = ax.figure
        ax.format_coord = self.format_coord

        self._norm = norm

        if data is None:
            if not ax.images:
                raise ValueError("No data found.")
            data = ax.images[-1].get_array()
        self.data = data = np.array(data, ndmin=3)
        self.slices = data.shape[0]
        self.ind = 0  # starting picture

        if self.slices > 1:
            ax.set_title("use scroll wheel to navigate images")
            # self.fig.suptitle('use scroll wheel to navigate images')

        imkwargs = dict(
            interpolation="nearest", origin="lower", norm=cnorm(data[self.ind], norm)
        )
        imkwargs.update(imshow_kw)
        self.clims = {}
        if not ax.images:
            self.im = ax.imshow(data[self.ind], **imkwargs)
            self.cb = plt.colorbar(self.im)
        else:
            self.im = ax.images[-1]

        if not hasattr(self, "cursor"):  # first time
            # print('switch on onclick',rectangle_onclick)
            self.cursor = Cursor(ax, useblit=True, color="red", linewidth=1)
            if not rectangle_onclick:
                self.fig.canvas.mpl_connect("button_release_event", self.onclick)
                print("switch on onclick")
            self.fig.canvas.mpl_connect("scroll_event", self.onscroll)
        self.update(True)

        self.exit_onclick = exit_onclick
        self.rectangle_onclick = rectangle_onclick

        if self.rectangle_onclick and not self.exit_onclick:
            self.rect = Rectangle((0, 0), 1, 1, facecolor="None", edgecolor="green")
            self.x0 = 0
            self.y0 = 0
            self.x1 = 0
            self.y1 = 0
            self.ax.add_patch(self.rect)
            self.toggle = False
            self.ax.figure.canvas.mpl_connect("button_press_event", self.on_press)
            self.ax.figure.canvas.mpl_connect("button_release_event", self.on_release)
            self.ax.figure.canvas.mpl_connect("motion_notify_event", self.on_motion)
        self.POI_list = []

    def set_extent(self, xmin, xmax, ymin, ymax):
        extent = (xmin, xmax, ymin, ymax)
        self.im.set_extent(extent)

    def set_percentile(self, lower, upper):
        for i, d in enumerate(self.data):
            vmin, vmax = image.percentile_interval(d, lower, upper)
            self.clims[i] = vmin, vmax

    def set_axes_properties(self, **prop):
        """
        This allows to define multiple properties for
        a matplotlib subplot which will be used for the different
        frames when scrolling the mouse wheel.
        """
        for k in prop:
            setter = "set_%s" % k
            val = prop[k]
            if not hasattr(self.ax, setter):
                continue
            if hasattr(val, "__iter__") and len(val) == self.slices:
                self._axes_properties[setter] = val
            else:
                self._axes_properties[setter] = [val] * self.slices

    def format_coord(self, x, y):
        xlabel = self.ax.xaxis.label._text
        ylabel = self.ax.yaxis.label._text
        ext = self.im._extent
        A = self.im._A
        ix = int((x - ext[0]) / (ext[1] - ext[0]) * A.shape[1])
        iy = int((y - ext[2]) / (ext[3] - ext[2]) * A.shape[0])
        ix = np.clip(ix, 0, A.shape[1] - 1)
        iy = np.clip(iy, 0, A.shape[0] - 1)
        I = A[iy, ix]
        return "%s=%1.4f, %s=%1.4f, I=%g" % (xlabel, x, ylabel, y, I)

    def onscroll(self, event):
        if self.slices == 1:
            return
        # print("%s %s" % (event.button, event.step))
        if event.button == "up":  # up should be previous
            ind = np.clip(self.ind - 1, 0, self.slices - 1)
        else:
            ind = np.clip(self.ind + 1, 0, self.slices - 1)

        if self.ind != ind:
            self.ind = ind
            self.update(props=True)

    def onclick(self, event):
        if not event.inaxes == self.ax:
            print("\nYou did not click on the display.")
            return
        xlabel = self.ax.xaxis.label._text
        ylabel = self.ax.yaxis.label._text
        xdata = event.xdata
        ydata = event.ydata
        print("You selected:    %s %.2f    %s %.2f" % (xlabel, xdata, ylabel, ydata))
        self.POI = xdata, ydata
        self.POI_mot_nm = xlabel, ylabel
        self.POI_list.append((xdata, ydata))
        # self.ax.annotate("p%i"%len(self.POI_list), xy=(ydata, xdata),)
        if self.exit_onclick:
            plt.close(self.fig)

    def on_press(self, event):
        self.x0 = event.xdata
        self.y0 = event.ydata
        self.x1 = event.xdata
        self.y1 = event.ydata
        try:
            self.rect.set_width(self.x1 - self.x0)
            self.rect.set_height(self.y1 - self.y0)
            self.rect.set_xy((self.x0, self.y0))
        except TypeError:
            print("You clicked outside the window - try again")

        self.rect.set_linestyle("dashed")
        self.ax.figure.canvas.draw()
        self.toggle = True

    def on_motion(self, event):
        if self.on_press is True:
            return
        if self.toggle:
            self.x1 = event.xdata
            self.y1 = event.ydata
            try:
                self.rect.set_width(self.x1 - self.x0)
                self.rect.set_height(self.y1 - self.y0)
                self.rect.set_xy((self.x0, self.y0))
            except TypeError:
                print("You moved the mouse outside the window - try again")
            self.rect.set_linestyle("dashed")
            self.ax.figure.canvas.draw()

    def on_release(self, event):
        if not event.inaxes == self.ax:
            print("\nYou did not click on the display.")
            self.on_press = False
            self.toggle = False
            return
        # print 'release'
        self.x1 = event.xdata
        self.y1 = event.ydata
        try:
            self.rect.set_width(self.x1 - self.x0)
            self.rect.set_height(self.y1 - self.y0)
            self.rect.set_xy((self.x0, self.y0))
        except TypeError:
            print("You clicked outside the window - try again")
        self.rect.set_linestyle("solid")
        self.ax.figure.canvas.draw()
        plt.close(self.fig)

    def update(self, props=False):
        if props:
            for k, v in self._axes_properties.items():
                getattr(self.ax, k)(v[self.ind])
        data = self.data[self.ind]
        self.im.set_data(data)
        vmin, vmax = self.clims.get(self.ind, (None, None))
        self.im.set_norm(cnorm(data, self._norm, vmin, vmax))
        self.fig.canvas.draw()


class GenericIndexTracker1D(object):
    """
    Just a matplotlibe widget that allows scrolling through
    several images and picking a point.
    No connection to spec.
    """

    _axes_properties = {}

    def __init__(
        self,
        ax,
        data=None,
        norm="linear",
        quantum=1.0,
        exit_onclick=False,
        rectangle_onclick=False,
    ):
        self.ax = ax
        self.fig = ax.figure
        # ax.format_coord = self.format_coord

        self._norm = norm

        if data is None:
            if not ax.lines:
                raise ValueError("No data found.")
            data = ax.lines[-1].get_array()
        self.data = data = np.array(data, ndmin=3)
        self.slices = data.shape[0]

        self.ind = 0  # starting picture

        if self.slices > 1:
            ax.set_title("use scroll wheel to navigate images")
            # self.fig.suptitle('use scroll wheel to navigate images')

        imkwargs = dict(
            interpolation="nearest", origin="lower", norm=cnorm(data[self.ind], norm)
        )
        if not ax.lines:
            self.ln = ax.plot(data[self.ind, :, 0])
            # self.cb = plt.colorbar(self.im)
        else:
            self.ln = ax.lines[-1]

        if not hasattr(self, "cursor"):  # first time
            self.cursor = Cursor(ax, useblit=True, color="red", linewidth=1)
            self.fig.canvas.mpl_connect("button_release_event", self.onclick)
            self.fig.canvas.mpl_connect("scroll_event", self.onscroll)
        self.update(True)

        self.exit_onclick = exit_onclick

        self.POI_list = []

    def set_axes_properties(self, **prop):
        """
        This allows to define multiple properties for
        a matplotlib subplot which will be used for the different
        frames when scrolling the mouse wheel.
        """
        for k in prop:
            setter = "set_%s" % k
            val = prop[k]
            if not hasattr(self.ax, setter):
                continue
            if hasattr(val, "__iter__") and len(val) == self.slices:
                self._axes_properties[setter] = val
            else:
                self._axes_properties[setter] = [val] * self.slices

    def format_coord(self, x, y):
        xlabel = self.ax.xaxis.label._text
        ylabel = self.ax.yaxis.label._text
        ext = self.im._extent
        A = self.im._A
        ix = int((x - ext[0]) / (ext[1] - ext[0]) * A.shape[1])
        iy = int((y - ext[2]) / (ext[3] - ext[2]) * A.shape[0])
        ix = np.clip(ix, 0, A.shape[1] - 1)
        iy = np.clip(iy, 0, A.shape[0] - 1)
        I = A[iy, ix]
        return "%s=%1.4f, %s=%1.4f, I=%g" % (xlabel, x, ylabel, y, I)

    def onscroll(self, event):
        if self.slices == 1:
            return
        # print("%s %s" % (event.button, event.step))
        if event.button == "up":  # up should be previous
            ind = np.clip(self.ind - 1, 0, self.slices - 1)
        else:
            ind = np.clip(self.ind + 1, 0, self.slices - 1)

        if self.ind != ind:
            self.ind = ind
            self.update(props=True)

    def onclick(self, event):
        if not event.inaxes == self.ax:
            print("\nYou did not click on the display.")
            return
        xlabel = self.ax.xaxis.label._text
        ylabel = self.ax.yaxis.label._text
        xdata = event.xdata
        ydata = event.ydata
        print("You selected:    %s %.2f  " % (xlabel, xdata))
        self.POI = xdata
        self.POI_mot_nm = xlabel
        self.POI_list.append((xdata, ydata))
        # self.ax.annotate("p%i"%len(self.POI_list), xy=(ydata, xdata),)
        if self.exit_onclick:
            plt.close(self.fig)

    def update(self, props=False):
        if props:
            for k, v in self._axes_properties.items():
                getattr(self.ax, k)(v[self.ind])
        data = self.data[self.ind, :, 0]
        self.ln[0].set_ydata(data)
        self.ax.set_ylim(min(data), max(data))
        self.fig.canvas.draw()
