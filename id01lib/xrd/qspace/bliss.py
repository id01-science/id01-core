import xrayutilities as xu
import numpy as np
import scipy.ndimage as ndi

from ..detectors import MaxiPix, MaxiPixGaAs, Eiger2M, Merlin
from ..geometries import ID01psic

from ...io.utils import correct_mpx1x4_gaps
from ...io.bliss import ioh5, get_counter, get_positioner, get_detector_aliases, _check_detector


_det_aliases = dict(
    mpx1x4=MaxiPix(), mpxgaas=MaxiPixGaAs(), eiger2M=Eiger2M(), Merlin=Merlin()
)


def _get_det_class(dset_h5f_opened, scan_no, detector):
    alias = _check_detector(dset_h5f_opened.filename, scan_no, detector)
    try:
        return _det_aliases[alias]
    except KeyError:
        msg = f"No alias is implemented for detector {alias}."
        raise Exception(msg)


@ioh5
def get_qspace_vals(
    path_dset,
    scan_numbers,
    geom=ID01psic(),
    usemotors=None,
    offsets=dict(),
    det=None,
    energy=None,
    cen_pix=None,
    det_dist=None,
    roi=None,
    ipdir=[1, 0, 0],
    ndir=[0, 0, 1],
    sample_orientation="z+",
    spherical=False,
    ignore_mpx_motors=True,
    verbose=True,
):
    """
    Calculate q-space values for a given scan or list of scans contained in a specified
    .hdf5 BLISS dataset.

    Parameters
    ----------
    path_dset : str
        Path to the .hdf5 BLISS dataset containing the scan data.
    scan_numbers : str, int, list, or tuple
        Scan number(s) for which to compute q-space coordinates. If a list of scan
        numbers is given, these are assumed to be contiguous, i.e. the positioner
        changing to perform the scan is increasing or decreasing monotonically.
    geom : ID01psic or object, optional
        Geometry object. Defaults to ID01psic().
    usemotors : list or None, optional
        List of diffractometer motors to be used for the q-space transformation.
        Defaults to None, i.e. all motors defined in the specified geometry are used.
    offsets : dict, optional
        Dictionary specifying offsets for the diffractometer motors. Defaults to an
        empty dictionary.
    det : str or None, optional
        Detector alias used for the scan(s). Defaults to None.
    energy : float or None, optional
        X-ray energy in keV. Defaults to None.
    cen_pix : tuple or None, optional
        Tuple specifying the detector central pixel coordinates. Defaults to None.
    det_dist : float or None, optional
        Detector distance in meters. Defaults to None.
    roi : list or None, optional
        Region of interest for the detector of the type
        [row_min, row_max, col_min, col_max]. Defaults to None.
    ipdir : list, optional
        Crystallographic direction of the sample parallel to the incident beam.
        Defaults to [1, 0, 0].
    ndir : list, optional
        Crystallographic direction of the sample parallel the z axis of the
        diffractometer, i.e. out of plane. Defaults to [0, 0, 1].
    sample_orientation : str, optional
        Orientation of the sample surface relative to the diffractometer coordinate
        frame. Defaults to "z+".
    spherical : bool, optional
        If True, returns spherical coordinates. Defaults to False.
    ignore_mpx_motors : bool, optional
        If True, central pixel correction for mpx motors is ignored. Defaults to True.
    verbose : bool, optional
        If True, prints verbose information. Defaults to True.

    Returns
    -------
    numpy.ndarray, numpy.ndarray, numpy.ndarray
        Arrays containing q-space values along qx, qy, and qz directions respectively.

    Raises
    ------
    KeyError
        If the specified scan number or motor name does not exist in the HDF5 file.
    """
    # NB path_dset is the opened hdf5 file

    # get some params
    if usemotors is None:
        usemotors = geom.usemotors
    if type(scan_numbers) in (list, tuple, range):
        if type(scan_numbers[0]) is str:
            scan_nos = scan_numbers
            scan_no = scan_nos[0]
        else:
            scan_nos = [f"{x}.1" for x in scan_numbers]
            scan_no = scan_nos[0]
    elif type(scan_numbers) is int:
        scan_nos = None
        scan_no = f"{scan_numbers}.1"
    elif type(scan_numbers) is str:
        scan_nos = None
        scan_no = scan_numbers

    # get detector and roi
    if det is None:
        det = _get_det_class(path_dset, scan_no, det)
    if type(det) == str:
        det = _det_aliases[det]
    if roi is None:
        roi = [0, det.pixnum[0], 0, det.pixnum[1]]

    # central pixel
    if cen_pix is None:
        cpy, cpx = [
            path_dset[f"{scan_no}/instrument/{det.alias}/beam_center_{p}"][()]
            for p in ("y", "x")  # note the letters! the id01 script uses wrong ones
        ]
    else:
        cpy, cpx = cen_pix
    if verbose:
        print(f"Using cen_pix: row/y={cpy:.3f}, col/x={cpx:.3f}")

    # detector distance
    if det_dist is None:
        detdist = path_dset[f"{scan_no}/instrument/{det.alias}/distance"][()]
    else:
        detdist = det_dist
    if verbose:
        print(f"Using det_dist = {detdist:.5f} m")

    # energy
    if energy is None:
        nrj = path_dset[f"{scan_no}/instrument/monochromator/WaveLength"][()]
        nrj = 12398e-10 / nrj  # eV
    else:
        nrj = energy * 1e3
    if verbose:
        print(f"Using energy = {nrj/1e3:.5f} keV")

    # correct central pixel for mpx translation motors
    if det.alias == "mpx1x4":
        mpxy, mpxz = [
            get_positioner(path_dset.filename, scan_no, m) for m in ("mpxy", "mpxz")
        ]
        if not ignore_mpx_motors:
            mpx_off = [offsets.get(a, 0) for a in ("mpxy", "mpxz")]
            if abs(mpxy) > 0.01 or abs(mpxz) > 0.01:
                cpx -= (mpxy - mpx_off[0]) / 1000.0 / det.pixsize[0]
                cpy += (mpxz - mpx_off[1]) / 1000.0 / det.pixsize[1]

                msg = f"Correcting for mpxy={mpxy-mpx_off[0]}, mpxz={mpxz-mpx_off[1]}"
                msg += f" ==> cen_pix: row/y={cpy:.3f}, col/x={cpx:.3f}"
                if verbose:
                    print(msg)
        else:
            if verbose:
                print(f"NOT correcting the central pixel for mpxy={mpxy}, mpxz={mpxz}")

    # scan angles names
    angles = geom.sample_rot.copy()
    angles.update(geom.detector_rot)

    # load angles
    if verbose:
        print("------------------")
    for a in angles:
        pos = get_positioner(path_dset.filename, scan_no, a) if a in usemotors else 0
        if scan_nos is not None:  # multi scans
            if a in usemotors:
                pos = np.concatenate(
                    [np.array([get_positioner(path_dset.filename, s, a)]) for s in scan_nos]
                )
            else:
                pos = 0
        off = offsets.get(a, 0)
        angles[a] = pos - off
        if off != 0:
            if verbose:
                print(f"Subtracting {off:.3f} from {a}")

    # Init the experiment class feeding it the geometry
    hxrd = xu.HXRD(
        ipdir,
        ndir,
        en=nrj,
        qconv=geom.getQconversion(),
        geometry="real",
        sampleor=sample_orientation,
    )

    # init XU detector class
    hxrd.Ang2Q.init_area(
        *det.directions,
        cch1=cpy,
        cch2=cpx,
        Nch1=det.pixnum[0],
        Nch2=det.pixnum[1],
        pwidth1=det.pixsize[0],
        pwidth2=det.pixsize[1],
        distance=detdist,
        roi=roi,
    )
    
    # Calculate q space values
    qx, qy, qz = hxrd.Ang2Q.area(*angles.values())  # TODO UB=hxrd._transform.matrix ?
    if spherical:
        _x, _y, _z = qy, qz, qx  # rotate coord system
        qz = np.sqrt(_x**2 + _y**2 + _z**2)  # radial
        qy = np.degrees(np.arccos(_z / qz))  # rot arount qy (first)
        qx = np.degrees(np.arctan2(_y, _x))  # rot around qx (second)

    return qx, qy, qz


@ioh5
def get_qspace_gridded(
    path_dset,
    scan_no,
    nbins=(-1, -1, -1),
    usemotors=None,
    offsets=dict(),
    det=None,
    geom=ID01psic(),
    energy=None,
    cen_pix=None,
    det_dist=None,
    roi=None,
    ipdir=[1, 0, 0],
    ndir=[0, 0, 1],
    sample_orientation="z+",
    spherical=False,
    ignore_mpx_motors=True,
    verbose=True,
    projection=None,
    median_filter=None,
    monitor=None,
    raw_det_gaps=False,
):

    # get detector
    if det is None:
        det = _get_det_class(path_dset, scan_no, det)

    # q-space coordinates
    qx, qy, qz = get_qspace_vals(
        path_dset.filename,
        scan_no,
        usemotors=usemotors,
        offsets=offsets,
        det=det,
        geom=geom,
        energy=energy,
        cen_pix=cen_pix,
        det_dist=det_dist,
        roi=roi,
        ipdir=ipdir,
        ndir=ndir,
        sample_orientation=sample_orientation,
        spherical=spherical,
        ignore_mpx_motors=ignore_mpx_motors,
        verbose=verbose,
    )

    # frames and their corrections
    if verbose:
        print("\nLoading frames into memory...", end=" ")
    if roi is not None:
        _roi = np.s_[:, roi[0] : roi[1], roi[2] : roi[3]]
    else:
        _roi = np.s_[...]
    frames = path_dset[f"/{scan_no}/measurement/{det.alias}"][_roi]
    if verbose:
        print("Done.")

    if det.mask is not None:
        mask = det.mask if det.mask.dtype == "bool" else det.mask.astype("bool")
        frames[:, mask == True] = 0
    if det.alias == "mpx1x4":
        for i in range(frames.shape[0]):
            frames[i] = correct_mpx1x4_gaps(frames[i], raw=raw_det_gaps)
    if median_filter is not None:
        frames = ndi.median_filter(frames, median_filter)
    if monitor is not None:
        mon = get_counter(path_dset.filename, scan_no, monitor).astype(frames.dtype)
        if not (mon > 0).all():
            raise ValueError("Found negative readings in monitor: %s" % monitor)
        else:
            mon = mon[:, None, None] * np.ones(
                (1, *frames.shape[1:]), dtype=frames.dtype
            )
            frames = frames / mon

    # compute max n of bins - TODO this is slow!
    maxbins = []
    safemax = lambda arr: arr.max() if arr.size else 0
    for dim in (qx, qy, qz):
        maxstep = max((safemax(abs(np.diff(dim, axis=j))) for j in range(3)))
        maxbins.append(int(abs(dim.max() - dim.min()) / maxstep))
    if verbose:
        print("\nMax. number of bins: (%i, %i, %i)" % tuple(maxbins))

    # get the dimensions to use
    _qdims = "xyz"
    if projection is None:  #
        idim = [0, 1, 2]
    elif len(projection) == 1 and projection in _qdims:
        idim = [_qdims.index(projection)]
    elif projection == "radial":
        idim = None
    elif len(projection) == 2 and projection[0] in _qdims and projection[1] in _qdims:
        idim = [_qdims.index(projection[i]) for i in (0, 1)]
    else:
        raise ValueError("Invalid input for projection: %s" % str(projection))

    # process input for number of bins
    if not hasattr(nbins, "__iter__"):
        nbins = [nbins]
    if idim is not None:  # not radial
        if all([b == -1 for b in nbins]):
            nbins = [int(maxbins[j]) for j in idim]
        elif all([b < 0 for b in nbins]):
            nbins = [int(maxbins[j] / abs(nbins[i])) for (i, j) in enumerate(idim)]
        elif all([b > 0 for b in nbins]):
            pass
        else:
            raise ValueError("Invalid input for nbins: %s" % str(nbins))
        toolarge = [(maxbins[j] < nbins[i]) for (i, j) in enumerate(idim)]
        if any(toolarge):
            il = toolarge.index(True)
            print("WARNING: number of bins exceeds maximum in q%s." % _qdims[idim[il]])
    else:
        if all([b == -1 for b in nbins]):
            nbins = [int(max(maxbins))]
    if verbose:
        print("Using binning: %s" % str(nbins))

    # convert data to regular grid in reciprocal space
    if idim == [0, 1, 2]:  # No projection
        gridder = xu.Gridder3D(*nbins)
        gridder(qx, qy, qz, frames)
        return (gridder.xaxis, gridder.yaxis, gridder.zaxis, gridder.data)

    elif projection == "radial" and not spherical:
        qabs = np.sqrt(qx**2 + qy**2 + qz**2)
        gridder = xu.Gridder1D(*nbins)
        gridder(qabs, frames)
        return (gridder.xaxis, gridder.data)

    elif len(idim) == 1:
        gridder = xu.Gridder1D(*nbins)
        gridder((qx, qy, qz)[idim[0]], frames)
        return (gridder.xaxis, gridder.data)

    elif len(idim) == 2:
        gridder = xu.Gridder2D(*nbins)
        gridder((qx, qy, qz)[idim[0]], (qx, qy, qz)[idim[1]], frames)
        return (gridder.xaxis, gridder.yaxis, gridder.data)
