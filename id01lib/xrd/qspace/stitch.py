"""
Functions to deal with scans where filtering was varied in a control manner
so that resulting images can be stitched together.
"""

import h5py
import numpy as np
import xrayutilities as xu
import scipy

from .detectors import MaxiPix  # default detector
from .geometries import ID01psic  # default geometry
from .qconversion import get_qspace_vals


def get_eta(h5file, sample, scan_no):
    with h5py.File(h5file, "r") as h5f:
        eta = h5f[sample + "/" + scan_no + ".1/measurement/eta"][()].astype(float)
    return eta


def get_data_roi(h5file, sample, scan_no, roi_name="roi2"):
    with h5py.File(h5file, "r") as h5f:
        data = h5f[sample + "/" + scan_no + ".1/measurement/image_0/data"][()].astype(
            "float32"
        )
        roi = h5f[sample + "/" + scan_no + ".1/measurement/{}".format(roi_name)][
            ()
        ].astype("float32")

        return data, roi


def stitch_scans(
    h5file,
    sample,
    scans,
    correct=False,
    roi_name="roi2",
    symmetric=False,
    large_gaps=False,
):
    imgs, roi2s = [], []

    # load the images and the rois for each scan
    for s in scans:
        img, roi = get_data_roi(h5file, sample, str(s), roi_name=roi_name)
        imgs.append(img)
        roi2s.append(roi)

    # calculate the ratio and correct each image by it
    if correct:
        if symmetric:
            N = len(scans)
        else:
            N = len(scans) - 1
        for index in range(1, N):
            ratio = roi2s[index - 1][-1] / roi2s[index][0]
            roi2s[index] *= ratio
            imgs[index][imgs[index] > 1] *= ratio

    # make a full stacked array for the scans
    final_array = np.concatenate([*imgs], axis=0)

    # correct imgs in stack for zeroes and hotpix
    for index in range(final_array.shape[0]):
        img = final_array[index, :, :]
        img[img == 0.0] = 1.0
        final_array[index, :, :] = img

    return final_array


def qconvert_stitched(
    fname,
    sample,
    image_data,
    scans,
    cen_pix_x,
    cen_pix_y,
    distance,
    nbins,
    geometry,
    maxbins=False,
    verbose=False,
    medfilter=False,
    monitor=None,
    detector=MaxiPix(),
    energy=8000,
    ipdir=[1, 0, 0],
    ndir=[0, 0, 1],
    roi=None,
    dtype="float32",
    detcorr=True,
):

    # Read out positioners from h5file (first scan in the series)
    motors = dict()
    with h5py.File(fname, "r") as h5f:
        sample_grp = h5f[str(sample)]
        pos0 = sample_grp["{}.1/instrument/positioners".format(scans[0])]
        for motor in pos0:
            motors[motor] = pos0[motor][()]

        # add mpx offset to central pixel (as meas from det_calib)
        cen_pix_y += motors["mpxz"] / 1000.0 / detector.pixsize[0]
        cen_pix_x -= motors["mpxy"] / 1000.0 / detector.pixsize[1]

        # initialise the experiment class feeding the ID01psic geometry to it
        hxrd = xu.HXRD(ipdir, ndir, en=energy, qconv=geometry.getQconversion())

        # select the whole detector as the roi if not specified
        if roi is None:
            roi = [0, detector.pixnum[0], 0, detector.pixnum[1]]

        # initalise the area detector
        hxrd.Ang2Q.init_area(
            detector.directions[0],
            detector.directions[1],
            cch1=cen_pix_x,
            cch2=cen_pix_y,
            Nch1=detector.pixnum[0],
            Nch2=detector.pixnum[1],
            pwidth1=detector.pixsize[0],
            pwidth2=detector.pixsize[1],
            distance=distance,
            roi=roi,
        )

        # get the angles of the ID01psic geometry
        angles = geometry.sample_rot.copy()
        angles.update(geometry.detector_rot)

        # Get eta values and stitch them
        eta_vals = []
        for scan in scans:
            eta_vals.append(
                sample_grp["{}.1/instrument/positioners/eta".format(scan)][()]
            )
        eta = np.concatenate((eta_vals))

        # total number of images in scan
        num_im = image_data.shape[0]

        # get monitor readings
        if monitor is not None:
            mon = np.array([])
            for scan in scans:
                mon = np.concatenate(
                    (mon, sample_grp["{}.1/measurement/{}".format(scan, monitor)][()])
                )
        else:
            mon = np.ones(num_im)
        if not (mon > 0).all():
            raise ValueError("Found negative readings in monitor: %s" % monitor)

    # Get the rest of the positioners and correct them for offsets
    maxlen = 1
    motors["eta"] = eta.astype(dtype)
    for angle in angles:
        if angle in geometry.usemotors:
            # fetch experimental angle value
            dset = motors[angle if angle is not "delta" else "del"]
            if len(
                dset.shape
            ):  # if it's 0 motor is still, if it's 1 motor is changing during scan
                maxlen = max(maxlen, dset.shape[0])
            position = dset
        else:
            position = 0.0
        angles[angle] = position - geometry.offsets[angle]

    if verbose:
        print("Offsets used: \n")
        for key, value in geometry.offsets.items():
            print("{0} = {1}".format(key, value))
    # output: ordered dict with offset corrected angles of interest used during a scan.

    # if the angle is kept constant during a scan, it is a scalar. If that's the case,
    # make it the same shape as the angle(s) which is being varied during a scan
    for angle in angles:
        if np.isscalar(angles[angle]):
            angles[angle] = np.ones(maxlen, dtype=np.float32) * angles[angle]

    # The actual conversion
    qx, qy, qz = hxrd.Ang2Q.area(*angles.values())
    qx, qy, qz = qx.astype(dtype), qy.astype(dtype), qz.astype(dtype)

    # calc max bins -- THIS IS SUPER SLOW?
    if maxbins:
        maxbins = []
        safemax = lambda arr: arr.max() if arr.size else 0
        for dim in (qx, qy, qz):
            maxstep = max((safemax(abs(np.diff(dim, axis=j))) for j in range(3)))
            maxbins.append(int(abs(dim.max() - dim.min()) / maxstep))
    else:
        maxbins = nbins
    print("Using {} bins".format(maxbins))

    # go through all images
    for idx in range(num_im):
        frame = image_data[idx] / mon[idx]
        if detcorr:
            detector.correct_image(frame)  # correct detector gaps
        if medfilter:  # kill some hot pixels
            frame = scipy.signal.medfilt2d(frame, [3, 3])

        if not idx:  # first iteration
            # create cube of empty data
            intensity = np.empty((num_im, frame.shape[0], frame.shape[1])).astype(dtype)
            intensity[idx] = frame.astype(dtype)
        else:
            intensity[idx, :, :] = frame.astype(dtype)

    # grid data
    gridder = xu.Gridder3D(*maxbins)
    gridder(qx, qy, qz, intensity)

    return (
        gridder.xaxis.astype(dtype),
        gridder.yaxis.astype(dtype),
        gridder.zaxis.astype(dtype),
        gridder.data.astype(dtype),
    )
