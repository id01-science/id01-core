import numpy as np


def mode1d(y, bins=100, integ_range=5, weights=None):
    """
    Spits out the `mode` (most frequent value) of a continuous
    1D distribution by discretization to a certain number of *bins*.

    To avoid discretization error, the max position is computed by
    via the center of mass of a +-range around the max.
    """

    y = np.ravel(y)
    y = y[~np.isnan(y)]

    yd, xd = np.histogram(y, bins=bins, weights=weights)
    xd = (xd[1] - xd[0]) / 2 + xd[:-1]  # centers of bins

    imax = yd.argmax()

    left = max(0, -integ_range + imax)
    right = min(len(xd) - 1, integ_range + imax)

    irange = np.arange(left, right)
    yd, xd = yd[irange], xd[irange]
    yd -= yd.min()

    pmax = (yd * xd).sum() / yd.sum()

    return pmax


def rebin1d(
    x,
    y,
    weights=None,
    bins=None,
    xmin=None,
    xmax=None,
    discard_empty=False,
    edges=False,
    return_all=False,
):
    """

    Function that averages unsorted data via histogramming. The data
    does not have to be present for repeatingly the same values of the
    independent variable and does not have to be ordered. Therefore, a
    rebinning to a new equi-distant x-axis takes place. No subpixel
    splitting is done.

    Typical usage:


        #q_random -- array of randomly spaced and unsorted q values
        #I -- Intensity at q_random
        #monitor -- monitor readings at q_random

        q_equidist, I_normalized = rebin1d(q_random, I, monitor, bins=400)


    If there are empty bins, they will carry the value `np.nan`.
    These can be discarded using the `discard_empty` key word argument.

    If `edges` is True: Return edges of the new bins instead of the
                        centers (results in +1 increased length)
    """
    # x = np.hstack(x)
    if np.ndim(x) > 1:
        x = np.ravel(x)
        y = np.ravel(y)
    if xmin is None:
        xmin = x.min()
    if xmax is None:
        xmax = x.max()
    ind = (x >= xmin) * (x <= xmax)
    x = x[ind]
    y = y[ind]
    if bins is None:
        bins = (x.max() - x.min()) / np.diff(np.sort(x)).max()
        bins = int(np.floor(bins))
    if weights is None:
        weights = np.ones(len(x))
    else:
        weights = np.ravel(weights)[ind]
    y, newx = np.histogram(x, bins=bins, weights=y, range=(xmin, xmax))
    num, newx = np.histogram(x, bins=bins, weights=weights, range=(xmin, xmax))
    dx = newx[1] - newx[0]
    x = newx[1:] - dx / 2.0
    y /= num
    ind = num > 0
    if not ind.all() and discard_empty:
        y = y[ind]
        x = x[ind]
    if edges:
        x = np.append(x[0] - dx / 2.0, x + dx / 2.0)
    if return_all:
        return dict(x=x, y=y, num=num, bins=bins)
    return x, y


def rebin2d(
    x1,
    x2,
    y,
    weights=None,
    bins=50,
    x1min=None,
    x1max=None,
    x2min=None,
    x2max=None,
    edges=False,
):
    """
    (Similar to Gridder2d)

    Function that averages unsorted data via histogramming.
    The input data can be given on an irregular grid and the order is
    not relevant.
    All input arrays should have the same .size and will be flattened.
    A rebinning to a new equi-distant x-axis takes place. No subpixel
    splitting is done.

    Typical usage:


        #q_x, q_y -- arrays of randomly spaced and unsorted q values
        #I -- Intensity at (q_x, q_y)
        #monitor -- monitor readings at (q_x, q_y)

        q_x_regular,
        q_y_regular,
        I_normalized = rebin1d(q_x, q_y, I, monitor, bins=100)


    If there are empty bins, they will carry the value `np.nan`.

    If `edges` is True: Return edges of the new bins instead of the
                        centers (results in +1 increased length)

    """
    x1 = np.ravel(x1)
    x2 = np.ravel(x2)
    y = np.ravel(y)

    if x1min is None:
        x1min = x1.min()
    if x1max is None:
        x1max = x1.max()
    if x2min is None:
        x2min = x2.min()
    if x2max is None:
        x2max = x2.max()

    ind = (x1 >= x1min) * (x1 <= x1max) * (x2 >= x2min) * (x2 <= x2max)
    if not ind.all():
        x1 = x1[ind]
        x2 = x2[ind]
        y = y[ind]

    if isinstance(bins, int):
        bins = [bins] * 2
    if len(bins) != 2:
        raise ValueError("Invalid input for `bins`")
    if weights is None:
        weights = np.ones_like(y)
    else:
        weights = np.ravel(weights)[ind]

    rng = ([x1min, x1max], [x2min, x2max])

    num, _, _ = np.histogram2d(x1, x2, weights=weights, bins=bins, range=rng)
    y, x1, x2 = np.histogram2d(x1, x2, weights=y, bins=bins, range=rng)

    y /= num
    if edges:
        return x1, x2, y

    dx1 = x1[1] - x1[0]
    dx2 = x2[1] - x2[0]

    x1 = x1[1:] - dx1 / 2.0
    x2 = x2[1:] - dx2 / 2.0
    return x1, x2, y

def Rz(ang, degrees=True):
    """
    Returns the rotation matrix for rotation around
    the z-axis according to the given angle in degrees per default.
    """
    if degrees:
        ang = np.radians(ang)
    One = np.ones_like(ang)
    Zero = np.zeros_like(ang)
    return np.array(
        [
            [np.cos(ang), -np.sin(ang), Zero],
            [np.sin(ang), np.cos(ang), Zero],
            [Zero, Zero, One],
        ]
    )


Rx = lambda ang, degrees=True: np.roll(Rz(ang, degrees), 1, (0, 1))
Ry = lambda ang, degrees=True: np.roll(Rz(ang, degrees), 2, (0, 1))

Rx.__doc__ = Rz.__doc__.replace("z-axis", "x-axis")
Ry.__doc__ = Rz.__doc__.replace("z-axis", "y-axis")


def cartesian2spherical(qx, qy, qz, degrees=True):
    """
    Definition of coordinate system when
    all diffractometer angles are zero:
    (see https://pasteboard.co/HmtPj91.png)
        qx -- beam direction
        qz -- vertical up
        qy right handed

        r -- length of q vector
        theta -- angle between q and qx
            (left handed rotation around qy)
        phi -- roll
            (left handed rotation around qx)
    """
    y = qz
    z = qx
    x = qy

    r = np.sqrt(qx ** 2 + qy ** 2 + qz ** 2)
    theta = np.arccos(z / r)
    phi = np.arctan2(y, x)

    if degrees:
        phi = np.degrees(phi)
        theta = np.degrees(theta)

    return dict(phi=phi, theta=theta, r=r)


def spherical2cartesian(phi, theta, r, degrees=True):
    """
    Definition of coordinate system when
    all diffractometer angles are zero:
    (see https://pasteboard.co/HmtPj91.png)
        qx -- beam direction
        qz -- vertical up
        qy right handed

        r -- length of q vector
        theta -- angle between q and qx
            (left handed rotation around qy)
        phi -- roll
            (left handed rotation around qx)
    """
    if degrees:
        theta = np.radians(theta)
        phi = np.radians(phi)

    vx = r * np.sin(theta) * np.cos(phi)
    vy = r * np.sin(theta) * np.sin(phi)
    vz = r * np.cos(theta)

    qx, qy, qz = vz, vx, vy

    return dict(qx=qx, qy=qy, qz=qz)


def unit_vector(vector):
    """Returns the unit vector of the vector."""
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
    """Returns the angle in radians between vectors 'v1' and 'v2'::

    >>> angle_between((1, 0, 0), (0, 1, 0))
    1.5707963267948966
    >>> angle_between((1, 0, 0), (1, 0, 0))
    0.0
    >>> angle_between((1, 0, 0), (-1, 0, 0))
    3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))
