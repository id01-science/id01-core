#----------------------------------------------------------------------
# Description: 
#   functions for analysing limatake multiple image acquisitions intensity fluctuations
#   crosscorrelation class - basler cameras only
#   analyse limatake for multiple images, intensity fluctuations
#   
# Author: Steven Leake <steven.leake@esrf.fr>
# Created at: Fri 09. Jun 23:20:30 CET 2017
# Computer: 
# System: 
#
#  20180413  analyse limatake is finished 
#
#  TODO: cross correlation class needs to be checked.
#  
#----------------------------------------------------------------------
#----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
import h5py
import pylab as pl
import sys
import os
from multiprocessing import Process, Lock, Queue

from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

from skimage.registration import phase_cross_correlation
from scipy.ndimage import fourier_shift
from scipy.ndimage import center_of_mass
from scipy.optimize import curve_fit

pixel_size = np.array([55, 55])  # microns



def gauss_func(x, a, x0, sigma):

    return a*np.exp(-(x-x0)**2/(2*sigma**2))

def analyse_limatake(h5fn, dict_rois, x = "/scan_0/instrument/detector_0/others/time_of_frame", y = "/scan_0/instrument/detector_0/data",group="/scan_0/analysis/",id=''):
    """
    You can generate a suitable hdf5 file with the following command
    
    silx convert --file-pattern align_mpx4_%05d.edf.gz --begin 15201 --end 20200 --compression -o BLfreqs.h5  
    
    dict_rois={"roi1":[0,0,516,0,516]}
    """
    
    colours = ['b','g','r','c','m','y','k','w','b','g','r','c','m','y','k','w','b','g','r','c','m','y','k','w'] 
    
    h5file=h5py.File(h5fn,'r')
    
    d_x = h5file[x].value
    #d_y = h5file[y].value
   
    pl.figure()
    tmp_y_list=[]
    ii=0
    for roi in dict_rois.keys():
        for ii in np.arange(d_x.shape[0]):
            tmp_y = h5file[y][ii,dict_rois[roi][3]:dict_rois[roi][4],dict_rois[roi][1]:dict_rois[roi][2]].sum()
            tmp_y_list.append(tmp_y)
			
        new_y=np.array(tmp_y_list)
        #pdb.set_trace()
        print(d_x.shape,new_y.shape)
        pl.plot(d_x, new_y/np.mean(new_y), label = roi)
        tmp_y_list=[]
	#h5w.add_dataset(h5fn,x,group,key = roi+"/x")
	#print(group+roi+"/x")
	#h5w.add_dataset(h5fn,tmp_y,group,key = roi+"/y")    
	#print(group+roi+"/x")   
        
    pl.legend()
    pl.xlabel('Time(s)')
    pl.ylabel('Normalised Intensity')
    pl.savefig('roi_vs_time_'+id+'.pdf')
    pl.clf()
    
    pl.figure()
    ii=0
    for roi in dict_rois.keys():
        for ii in np.arange(d_x.shape[0]):
            tmp_y = h5file[y][ii,dict_rois[roi][3]:dict_rois[roi][4],dict_rois[roi][1]:dict_rois[roi][2]].sum()
            tmp_y_list.append(tmp_y)
		
        new_y=np.array(tmp_y_list)						
        N = new_y.shape[0]
        T = np.round(d_x[1]-d_x[0],2)
        yf = np.fft.fft(new_y)
        xf = np.linspace(0.0,1.0/(2.0*T),N/2)
        #print(xf[:],(2.0/N*np.abs(yf[:int(N/2)]))
        pl.plot(xf[:],2.0/N*np.abs(yf[:int(N/2)]),label = roi)
        #h5w.add_dataset(h5fn,xf,group,key = roi+"/xf")
        #h5w.add_dataset(h5fn,yf,group,key = roi+"/yf")  
    pl.xlim(0.1, xf[-1])
    pl.ylim(0,np.max(2.0/N*np.abs(yf[2:int(N/2)])))
    pl.legend()
    pl.title('Beam Frequencies')
    pl.xlabel('Frequency (Hz)')
    pl.ylabel('Intensity (a.u)')
    pl.savefig('roi_freqs_'+id+'.pdf')
    pl.clf()
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    im = ax.imshow(np.sum(h5file[y],axis=0),norm=LogNorm())
    plt.colorbar(im)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    currentAxis = plt.gca()
    for i,roi in enumerate(dict_rois.keys()):
        currentAxis.add_patch(Rectangle((dict_rois[roi][1], dict_rois[roi][3]), dict_rois[roi][2]-dict_rois[roi][1], dict_rois[roi][4]-dict_rois[roi][3], fill= None, alpha=1,edgecolor=colours[i]))#, label='roi1')
        ax.annotate(roi, xy=(dict_rois[roi][1], dict_rois[roi][3]), xycoords='data',\
                    xytext=(-30,15), textcoords='offset points',\
                    arrowprops=dict(arrowstyle="->"))
    plt.savefig('sum_images_'+id+'.pdf')
    plt.clf()


def CCworker(ref_data, tmp_data, time_stamp, arrTimestamp, dump):
        """worker function for cross correlation class"""
        # do the cross correlation
        cc, error, diffphase = phase_cross_correlation(ref_data,tmp_data, upsample_factor=1000)
        x=cc[0] 
        y=cc[1] 
        # print x,y, time_stamp-arrTimestamp
        sys.stdout.write('.')
        # define your output
        dump.put([x, y, time_stamp-arrTimestamp])
        return


########################
# CLASS
########################

class CrossCorrelator():

    def __init__(self, refArr, list3Darr, listTimestamp, h5fn="CC.h5", roi=[[0, 966], [0, 1296]]):

        self.h5fn = h5fn
        self.roi = roi

        self.refArr = refArr[self.roi[0][0]:self.roi[0][1], self.roi[1][0]:self.roi[1][1]]
        plt.figure()
        plt.imshow(self.refArr)
        plt.show()
        self.listArr = []  # list
        for arr in list3Darr:
            self.listArr.append(arr[:,self.roi[0][0]:self.roi[0][1], self.roi[1][0]:self.roi[1][1]])

        self.output = np.zeros(0, dtype=[('x', 'f8'), ('y', 'f8'), ('timestamp', 'f8')])
        self.output1 = np.zeros(0, dtype=[('timestamp', 'f8'), ('max', 'f8'), ('sum', 'f8'),
                                          ('com_x', 'f8'), ('com_y', 'f8'), ('peak_x', 'f8'),
                                          ('peak_y', 'f8'),]) #('mpx1x4', 'f8,f8,f8,f8')])

        self.listTimestamp = listTimestamp
        self.time_zero = self.listTimestamp[0][0]

    def multi_CC(self, arr, arrTimeStamp ,lim_A=250, lim_B=10000, output=np.zeros(0, dtype=[('x', 'f8'),
                                                                                ('y', 'f8'),
                                                                                ('timestamp', 'f8')]), stats=False):
        """do CC on an image stack with multiple processors return the CC, as a structured array x,y,timestamp
        lim_A = should never exceed 250 on 16 cores w/ 32GB memory
        lim_B = defines the number of images you want to cross correlate
        """
        _dump = Queue()
        _tot_ims = output.shape[0]
        # print 'totims:',_tot_ims
        _l = Lock()
        _processes = []
        # if stats:
        # self.output1 = np.zeros(0,dtype = [('timestamp', 'f8'), ('max', 'f8'),
        # ('sum', 'f8'), ('com_x', 'f8'), ('com_y', 'f8'), ('peak_x', 'f8'), ('peak_y', 'f8')])

        ii = 0
        _no_ims2CC = arr.shape[0]

        _l.acquire()
        sys.stdout.write('%i slices in array\n' % (_no_ims2CC))
        sys.stdout.write('%i:%i of %i\n' % (ii, _tot_ims+_no_ims2CC, lim_B))
        _l.release()
        for ii in np.arange(_no_ims2CC):
            _l.acquire()
            sys.stdout.write('%i,' % (ii))
            _l.release()
            
            # get the data
            _im2CC = arr[ii,:,:]
            _timestamp = arrTimeStamp[ii]

            #print(_im2CC, _timestamp)
            # other stats
            if stats:
                stat = self.compress_xy_fitgauss_max_sum(_im2CC, _timestamp)
                self.output1 = np.r_[self.output1, np.array([(stat[0], stat[1], stat[2],
                                                              stat[3], stat[4], stat[5],
                                                              stat[6])], # (stat[7][0], stat[7][1],stat[7][2], stat[7][3]))],
                                                            dtype=[('timestamp', 'f8'), ('max', 'f8'), ('sum', 'f8'),
                                                                   ('com_x', 'f8'), ('com_y', 'f8'), ('peak_x', 'f8'),
                                                                   ('peak_y', 'f8'),])] # ('mpx1x4', 'f8,f8,f8,f8')])]

            # send to multiple processors
            p = Process(target=CCworker, args=(self.refArr, _im2CC, _timestamp, self.time_zero, _dump))
            p.start()
            _processes.append(p)

            # dump the data every time you hit the limit
            if ii % lim_A == lim_A-1 and ii != 0:
                sys.stdout.write('\n%i : %i\n' % (ii+_tot_ims, lim_B))
                for p in _processes:
                    p.join()

                # sys.stdout.write('Retrieving output\n')

                output = self.dump_queue(_dump, output=output)

        # if you have a non integer * limit number of points catch the leftovers and save them
        # if ii == _data.NumImages-1:
        #    sys.stdout.write('\n%i : %i\n' % (ii, lim_A))
        for p in _processes:
            p.join()

        # sys.stdout.write('\nRetrieving output\n')

        # iter = xrange(ii-ii % lim_A, ii)
        output = self.dump_queue(_dump, output=output)
        # update tot image no.
        _tot_ims += ii
        if stats:
            return output, self.output1
        else:
            return output

    def multi_arr_multi_CC(self, lim_A=250, lim_B=10000,
                          output=np.zeros(0, dtype=[('x', 'f8'),
                                                    ('y', 'f8'),
                                                    ('timestamp', 'f8')]), stats=False):
        if stats:
            for arr,arrTimestamp in zip(self.listArr, self.listTimestamp):
                self.output, self.output1 = self.multi_CC(arr, arrTimestamp,  lim_A, lim_B, output=self.output, stats=stats)
            return self.output, self.output1
        else:
            for arr,arrTimestamp in zip(self.listArr, self.listTimestamp):
                self.output = self.multi_CC(arr, arrTimestamp,  lim_A, lim_B, output=self.output, stats=stats)
            return self.output

    def dump_queue(self, queue, output=np.zeros(0, dtype=[('x', 'f8'),
                                                          ('y', 'f8'),
                                                          ('timestamp', 'f8')])):
        while True:
            # print queue.qsize()
            if int(queue.qsize()) == 0:
                break
            a = queue.get()
            output = np.r_[output, np.array([(a[0], a[1], a[2])],
                                            dtype=[('x', 'f8'), ('y', 'f8'), ('timestamp', 'f8')])]
        # print output
        return output

    def compress_xy_fitgauss_max_sum(self, tmp_data, _timestamp):
        x1 = np.linspace(0, tmp_data.shape[0]-1, tmp_data.shape[0])
        data_x = tmp_data.sum(axis=1)
        # print x1.shape,data_x.shape
        x2 = np.linspace(0, tmp_data.shape[1]-1, tmp_data.shape[1])
        data_y = tmp_data.sum(axis=0)
        # print x2.shape,data_y.shape

        com_x, com_y = center_of_mass(tmp_data)
        # fake data
        # data_x= gauss_func(x1,1,420,20)
        # data_y= gauss_func(x2,1,67,15)
        # data_y=np.where(data_y<0.05,0,data_y)
        # x2=np.where(data_y<0.05,0,x2)

        # Plot out the current state of the data and model
        # fig = pl.figure()
        # ax1 = fig.add_subplot(211)
        # ax1.plot(x1, data_x, c='b', label='integrated along y')
        # ax2 = fig.add_subplot(212)
        # ax2.plot(x2, data_y, c='b', label='integrated along x')
        # fig.savefig('model_and_noise.png')

        try:
            # Executing curve_fit
            popt1, pcov1 = curve_fit(gauss_func, x1, data_x)
            # y1 = gauss_func(x1, popt1[0], popt1[1], popt1[2])
            popt2, pcov2 = curve_fit(gauss_func, x2, data_y)
            # y2 = gauss_func(x2, popt2[0], popt2[1], popt2[2])

            # print popt # a,x0,sigma
            # ax1.plot(x1, y1, c='r', label='Best fit')
            # ax2.plot(x2, y2, c='r', label='Best fit')
            # ax1.legend()
            # ax2.legend()
            # fig.savefig('model_fit.png')
        except:
            # print fn, ' not fitted'
            popt1 = [0., 0., 0.]
            popt2 = [0., 0., 0.]

        # output
        return [_timestamp-self.time_zero, tmp_data.max(),
                tmp_data.sum(), com_x, com_y, popt1[1], popt2[1]]
    
        #        [tmp_data.sum(), tmp_data.sum(),
        #         tmp_data.sum(), tmp_data.sum()]]

    def dump_hdf5(self,):
        ID01_h5 = h5py.File(self.h5fn, 'w')
        if self.output.shape != 0:
            self.output.sort(order = 'timestamp')
            ID01_h5['cc_x'] = self.output['x']
            ID01_h5['cc_y'] = self.output['y']
            ID01_h5['timestamp'] = self.output['timestamp']
            ID01_h5['output_cc'] = self.output
        if self.output1.shape != 0:
            self.output1.sort(order = 'timestamp')
            ID01_h5['max'] = self.output1['max']
            ID01_h5['sum'] = self.output1['sum']
            ID01_h5['com_x'] = self.output1['com_x']
            ID01_h5['com_y'] = self.output1['com_y']
            ID01_h5['peak_x'] = self.output1['peak_x']
            ID01_h5['peak_y'] = self.output1['peak_y']
            #ID01_h5['mpx1x4'] = self.output1['mpx1x4']
            ID01_h5['timestamp_stats'] = self.output1['timestamp']
            ID01_h5['output_stats'] = self.output1
        ID01_h5.close()

    def plot_pdf(self, fn="peak_pos_cc.pdf", pixel_size=np.array([20, 20])):
        pl.figure(1)
        pl.plot(self.output['timestamp'], self.output['x']*pixel_size[0], 'b', label="Peak_x")
        pl.plot(self.output['timestamp'], self.output['y']*pixel_size[1], 'r--', label="Peak_y")
        pl.xlabel("Time(s)")
        pl.ylabel("Position (um)")
        pl.legend()
        pl.savefig(fn)
        print("peak(x) mean:", np.mean(self.output['x']*pixel_size[0]), "std: ", \
            np.std(self.output['x']*pixel_size[0]))
        print("peak(y) mean:", np.mean(self.output['y']*pixel_size[1]), "std: ", \
            np.std(self.output['y']*pixel_size[1]))
        pl.clf()

    def plot_fft_quelquechose(self, key='', fmt='.pdf', show=True):
        # fast for interactive file browsing
        ID01_h5 = h5py.File(self.h5fn, 'r')
        if key == '':
            print(ID01_h5.keys())
            key = raw_input('provide a key from the list above: ')

        pl.figure(1)
        breakpoint()
        # we have irregular data - beware really should be doing a non-uniform fft
        x = ID01_h5['timestamp_stats'][()]
        #deltas = [x[i]-x[i-1] for i in range(1,len(x[:]))]
        deltas=[]
        for i in range(1,len(x)-1):
            delta = x[i]-x[i-1]
            deltas.append(delta)

        x1 = np.linspace(0.0,1.0/(2.0*(x[1]-x[0])), int(x.shape[0]/2))
        y1 = np.abs(np.fft.fft(ID01_h5[key][:]))[:int(x.shape[0]/2)]
        pl.plot(x1,  y1, 'r--', label=key)
        pl.title(self.h5fn)
        pl.xlabel("Freq (Hz)")
        pl.ylabel("Intensity")
        pl.legend()
        pl.xlim(0.1, x1[-1])
        pl.ylim(0,y1[5:].max())
        out_fn = self.h5fn.split('.')[0]+'_'+key+fmt
        print(out_fn)
        if show:
            pl.show()
        else:
            pl.savefig(out_fn)
        pl.clf()

    def plot_quelquechose(self, key='', fmt='.pdf', show=True,normalise=False):
        # fast for interactive file browsing
        ID01_h5 = h5py.File(self.h5fn, 'r')
        if key == '':
            print(ID01_h5.keys())
            key = raw_input('provide a key from the list above: ')

        pl.figure(1)
        x = ID01_h5['timestamp_stats']
        if normalise:
            pl.plot(x[:ID01_h5[key].shape[0]]-x[0], ID01_h5[key][:]/np.mean(ID01_h5[key][:]), 'r--', label=key)
            pl.ylabel("Intensity normalised by mean")

        else:
            pl.plot(x[:ID01_h5[key].shape[0]]-x[0], ID01_h5[key][:], 'r--', label=key)
            pl.ylabel("Intensity")

        pl.title(self.h5fn)
        pl.xlabel("Time(s)")
        pl.legend()
        #pl.xlim(0.1, x[-1])
        out_fn = self.h5fn.split('.')[0]+'_'+key+fmt
        print(out_fn)
        if show:
            pl.show()
        else:
            pl.savefig(out_fn)
        pl.clf()


def plot_max(h5fn, out_fn="max.pdf"):

    ID01_h5 = h5py.File(h5fn, 'r')
    pl.figure(1)
    x = ID01_h5['timestamp_stats']
    pl.plot(x[:ID01_h5['max'].shape[0]]-x[0], ID01_h5['max'][:]/np.mean(ID01_h5['max'][:]), 'b', label="max")
    pl.title(h5fn)
    pl.xlabel("Time(s)")
    pl.ylabel("Intensity (normalised)")
    pl.legend()
    pl.savefig(out_fn)
    pl.clf()


def plot_sum(h5fn, out_fn="sum.pdf"):

    ID01_h5 = h5py.File(h5fn, 'r')
    pl.figure(1)
    x = ID01_h5['timestamp_stats']
    pl.plot(x[:ID01_h5['sum'].shape[0]]-x[0], ID01_h5['sum'][:]/np.mean(ID01_h5['sum'][:]), 'b', label="sum")
    pl.title(h5fn)
    pl.xlabel("Time(s)")
    pl.ylabel("Intensity (normalised)")
    pl.legend()
    pl.savefig(out_fn)
    pl.clf()


def plot_com(h5fn, out_fn="com.pdf", pix_size=[20, 20]):

    ID01_h5 = h5py.File(h5fn, 'r')
    pl.figure(1)
    x = ID01_h5['timestamp_stats']
    pl.plot(x[:ID01_h5['com_x'].shape[0]]-x[0], (ID01_h5['com_x'][:]-ID01_h5['com_x'][0])*pix_size[0], 'b', label="com_x")
    pl.plot(x[:ID01_h5['com_y'].shape[0]]-x[0], (ID01_h5['com_y'][:]-ID01_h5['com_y'][0])*pix_size[1], 'r', label="com_y")
    pl.title(h5fn)
    pl.xlabel("Time(s)")
    pl.ylabel("Deviation (Microns)")
    pl.legend()
    pl.savefig(out_fn)
    pl.clf()


def plot_peak(h5fn, out_fn="peak.pdf", pix_size=[20, 20]):

    ID01_h5 = h5py.File(h5fn, 'r')
    pl.figure(1)
    x = ID01_h5['timestamp_stats']
    pl.plot(x[:ID01_h5['peak_x'].shape[0]]-x[0], (ID01_h5['peak_x'][:]-ID01_h5['peak_x'][0])*pix_size[0], 'b', label="peak_x")
    pl.plot(x[:ID01_h5['peak_y'].shape[0]]-x[0], (ID01_h5['peak_y'][:]-ID01_h5['peak_y'][0])*pix_size[1], 'r', label="peak_y")
    pl.title(h5fn)
    pl.xlabel("Time(s)")
    pl.ylabel("Deviation (Microns)")
    pl.legend()
    pl.savefig(out_fn)
    pl.clf()


def plot_fft_quelquechose(h5fn, key='', fmt='.pdf', show=True):
    # fast for interactive file browsing
    ID01_h5 = h5py.File(h5fn, 'r')
    if key == '':
        print(ID01_h5.keys())
        key = raw_input('provide a key from the list above: ')

    pl.figure(1)
    breakpoint()
    # we have irregular data - beware really should be doing a non-uniform fft
    x = ID01_h5['timestamp_stats']
    x1 = np.linspace(0.0,1.0/(2.0*(x[1]-x[0])), int(x.shape[0]/2))
    y1 = np.abs(np.fft.fft(ID01_h5[key][:]))[:int(x.shape[0]/2)]
    pl.plot(x1,  y1, 'r--', label=key)
    pl.title(h5fn)
    pl.xlabel("Freq (Hz)")
    pl.ylabel("Intensity")
    pl.legend()
    pl.xlim(0.1, x1[-1])
    pl.ylim(0,y1[5:].max())
    out_fn = h5fn.split('.')[0]+'_'+key+fmt
    print(out_fn)
    if show:
        pl.show()
    else:
        pl.savefig(out_fn)
    pl.clf()

def plot_quelquechose(h5fn, key='', fmt='.pdf', show=True):
    # fast for interactive file browsing
    ID01_h5 = h5py.File(h5fn, 'r')
    if key == '':
        print(ID01_h5.keys())
        key = raw_input('provide a key from the list above: ')

    pl.figure(1)
    x = ID01_h5['timestamp_stats']
    pl.plot(x[:ID01_h5[key].shape[0]]-x[0], ID01_h5[key][:]/np.mean(ID01_h5[key][:]), 'r--', label=key)
    pl.title(h5fn)
    pl.xlabel("Time(s)")
    pl.ylabel("Intensity")
    pl.legend()
    #pl.xlim(0.1, x[-1])
    out_fn = h5fn.split('.')[0]+'_'+key+fmt
    print(out_fn)
    if show:
        pl.show()
    else:
        pl.savefig(out_fn)
    pl.clf()

########################
# CODE
########################



"""
from id01.process.h5utils import openScan                                                                          
from id01.process.crosscorrelation import CrossCorrelator                                                          
filename = "/data/visitor/blc14773/id01/20230829/RAW_DATA/copper/copper_ALIGNMENT_0003/copper_ALIGNMENT_0003.h5"   

datalist = []
timestamplist = []

for i in range(1,5):
    scan = openScan(filename, i) 
    datalist.append(scan.getCounter("lima_bv4") ) 
    if i==1:
        time_zero = scan.getCounter("epoch")[0]

    timestamplist.append(scan.getCounter("epoch")-time_zero)

cc=CrossCorrelator(datalist[0][0],datalist,timestamplist,roi=[[300,500],[500,800]])                                                                               
cc.multi_arr_multi_CC(stats=True)
cc.dump_hdf5()
    
"""
"""
scan = openScan(filename, 49)                                                                                      
data = scan.getCounter("lima_bv4")          
timeStamp=scan.getCounter("elapsed_time")                                                                         
cc=CrossCorrelator(data[0],[data],[timeStamp],roi=[[300,500],[500,800]])                                                                               
cc.multi_arr_multi_CC(stats=True)
cc.dump_hdf5()
"""            