import gif
import numpy as np
import os
import matplotlib.pyplot as plt

from tqdm.notebook import tqdm
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable

from .bliss import (
    get_scan_shape,
    get_command,
    get_positioner,
    get_detector_frames,
)


def correct_mpx1x4_gaps(frame, raw=False):
    """
    frame is 2D.
    """
    if not raw:
        i0, i1 = 255, 260
        n = 3
        div = 3
    else:
        i0, i1 = 254, 261
        n = 4
        div = 1

    frame[i0 : i0 + n, :] = (frame[i0, :] / div)[None, :]
    frame[i1 - n : i1, :] = (frame[i1, :] / div)[None, :]

    frame[:, i0 : i0 + n] = (frame[:, i0] / div)[:, None]
    frame[:, i1 - n : i1] = (frame[:, i1] / div)[:, None]

    return frame


def scan2gif(
    path_dset,
    scan_no,
    roi=None,
    clims=[None, None],
    dpi=90,
    fname=None,
    gif_duration=5,
    detector=None,
    cmap='inferno',
):
    """
    roi = [ymin, ymax, xmin, xmax]
    clims = [zmin, zmax] fixed, if None autoscale
    """

    if roi is None:
        roi = np.s_[:, :, :]
    elif type(roi) == list and len(roi) == 4:
        roi = np.s_[:, roi[0] : roi[1], roi[2] : roi[3]]
    else:
        raise TypeError("roi has to be a list [ymin, ymax, xmin, xmax]")

    if clims != [None, None]:
        if type(clims) == list and len(clims) == 2:
            pass
        else:
            raise TypeError("clims has to be a list [zmin, zmax]")

    cmd = get_command(path_dset, scan_no)
    sh = get_scan_shape(path_dset, scan_no)

    if type(sh) != int:
        raise Exception(f"Selected scan is not a 1D scan. Command: {cmd}")

    data = get_detector_frames(path_dset, scan_no, detector=detector)[roi]
    if "loop" in cmd:
        name_mot = "elapsed_time"
        moving_motor = get_positioner(path_dset, scan_no, name_mot)
    else:
        name_mot = cmd.split(" ")[1]  # TODO implement second motor for a2scans
        moving_motor = get_positioner(path_dset, scan_no, f"{name_mot}")

    @gif.frame
    def plt_img(i):
        fig, ax = plt.subplots(1, 1, figsize=(4, 3), layout="tight", dpi=dpi)

        im = ax.imshow(data[i], norm=LogNorm(*clims), origin="upper", cmap=cmap)
        ax.set_title(
            f"{os.path.basename(path_dset)} #{scan_no}\n {name_mot}:"
            f"{moving_motor[i]:.3f}"
        )
        cax = make_axes_locatable(ax).append_axes("right", size="3%", pad=0.05)
        cax.tick_params(labelsize="small")
        _ = fig.colorbar(im, cax=cax)

    frames = []
    for i in tqdm(range(data.shape[0])):
        frames.append(plt_img(i))

    if fname is None:
        fname = f"GIF_{os.path.basename(path_dset)}_{scan_no}.gif"
    gif.save(
        frames,
        fname,
        duration=gif_duration,
        unit="seconds",
        between="startend",
    )
