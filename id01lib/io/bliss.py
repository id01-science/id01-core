import os
import h5py

from functools import wraps
from datetime import datetime


def ioh5(func):
    @wraps(func)  # to get docstring of dectorated func
    def wrapper(filename, *args, **kwargs):
        with h5py.File(filename, "r") as h5f:
            return func(h5f, *args, **kwargs)

    return wrapper


@ioh5
def get_scan_shape(h5f, scan_no):
    """
    Retrieve the shape of the scan data based on the scan command stored
    in the .hdf5 BLISS dataset.

    Parameters
    ----------
    h5f : str
        Path to the .hdf5 BLISS dataset containing the scan data.
    scan_no : str
        The scan number for which to retrieve the scan shape, e.g. 1.1.

    Returns
    -------
    List[int] or int
        The scan shape.
    """

    cmd = h5f[f"{scan_no}/title"][()].decode()
    cmd_list = cmd.split(" ")

    if "sxdm" in cmd:
        sh = [h5f[f"{scan_no}/technique/{x}"][()] for x in ("dim0", "dim1")][::-1]
    elif "mesh" in cmd:
        sh = [int(cmd_list[x]) + 1 for x in (4, 8)][::-1]
    elif "scan" in cmd:
        sh = h5f[f"{scan_no}/measurement/acq_time"].shape[0]
    else:
        raise Exception(f"Scan command syntax not recognised: {cmd}")

    return sh


@ioh5
def get_positioner(h5f, scan_no, motor_name):
    """
    Retrieve the value of a given positioner from a .hdf5 BLISS dataset.

    Parameters
    ----------
    h5f : str
        Path to the .hdf5 BLISS dataset containing the scan data.
    scan_no : str
        The scan number, e.g. 1.1.
    motor_name : str
        The name of the positioner whose value is to be retrieved.

    Returns
    -------
    numpy.ndarray
        The value of the specified positioner as a NumPy array.
    """

    return h5f[f"/{scan_no}/instrument/positioners/{motor_name}"][()]


@ioh5
def get_counter(h5f, scan_no, counter_name):
    return h5f[f"{scan_no}/measurement/{counter_name}"][()]


@ioh5
def get_command(h5f, scan_no):
    return h5f[f"{scan_no}/title"][()].decode()


@ioh5
def get_startendtime(h5fname, scan_no):
    global samplename

    start, end = [h5fname[f"/{scan_no}/{x}_time"][()] for x in ("start", "end")]
    start, end = start.decode(), end.decode()
    start, end = [datetime.fromisoformat(x) for x in (start, end)]
    start, end = [x.strftime("%a %b %d %Y %H:%M:%S") for x in (start, end)]

    return start, end


# TODO check for NXDetector attr and raise error if not in alias
# list (i.e. one for which we have a id01lib.xrd.detector class)
@ioh5
def get_detector_aliases(h5f, scan_no):
    groups = tuple(h5f[f"{scan_no}/instrument/"].keys())
    aliases = ("mpx1x4", "mpxgaas", "eiger2M", "detector", "merlin", "andor_zyla")
    detectors = [d for d in aliases if d in groups]

    if len(detectors) > 0:
        return detectors
    else:
        fname = os.path.basename(h5f.filename)
        msg = f"No detector group found in {fname}:/{scan_no}/instrument/"
        raise Exception(msg)


@ioh5
def get_roi_names(h5f, scan_no, only_sum=False):
    aliases = get_detector_aliases(h5f.filename, scan_no)
    counters = list(h5f[f"{scan_no}/measurement"].keys())

    roi_names = [n for n in counters if any([s in n for s in aliases])]
    [roi_names.remove(n) for n in aliases if n in roi_names]

    if only_sum:
        roi_names = [
            x for x in roi_names if not any([s in x for s in "avg,max,min,std".split(",")])
        ]

    return roi_names


@ioh5
def get_detector_frames(h5f, scan_no, detector=None):
    """
    Works for 1D scans only
    """

    detector = _check_detector(h5f.filename, scan_no, detector)
    data = h5f[f"{scan_no}/measurement/{detector}/"][()]

    return data


def _check_detector(path_dset, scan_no, detector):
    det_list = get_detector_aliases(path_dset, scan_no)

    if detector is None:
        detector = det_list[0]
        if len(det_list) > 1:
            print(
                f"> Found multiple detectors: {det_list}. Using '{detector}'. "
                "Select a different one via the 'detector=' keyword argument."
            )

    if detector not in det_list:
        raise Exception(f"Detector '{detector}' not found. Try setting detector=None.")

    return detector
