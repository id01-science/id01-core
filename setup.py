from distutils.core import setup
import setuptools

setup(
    name="id01-core",
    version="0.1.0",
    author="E. Zatterin",
    author_email="edoardo.zatterin@esrf.fr",
    long_description=open("README.md").read(),
    packages=[
        "id01lib",
        "id01lib.xrd",
        "id01lib.xrd.qspace",
        "id01lib.plot",
        "id01lib.process",
        "id01lib.io",
        "id01lib.io.id01h5"
    ],
    install_requires=[
        "numpy >= 1.1.1",
        "matplotlib",
        "silx",
        "wheel"
    ],
    scripts = [
        "bin/CamView",
    ],
)

if __name__ == "main":
    setuptools.setup()
